export const JobUris = {
  LIST_PAGE: '/page/job/list',
};

export const FunctionUris = {
  READ_PAGE: '/page/func/read',
  STATUS_PAGE: '/page/func/status',
  LIST_PAGE: '/page/func/list',
  GET_WORDS_RANDOMLY: '/word/random',
  GET_WORDS_STATUS: '/word/query',
  GET_WORDS_LIST: '/word/queryAll',
  UPDATE_WORDS_CLICK: '/word/update-count/save',
  CLEAR_WORDS_CLICK: '/word/update-count/clear',
};

export const CommonUris = {
  HOME_PAGE: '/page/home'
};