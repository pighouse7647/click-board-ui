import {FunctionEvents} from '../constants/Events';

let word;

function getCallbackfn() {
  return (m, o) => {
    let originObj = m.get(o.userId);
    if (!originObj) {
      m.set(o.userId, {userId: o.userId, data: [o]});
    } else {
      m.set(o.userId, {userId: o.userId, data: [...(originObj.data), o]});
    }
    return m;
  };
}

word = (state = {list: [], entityUpdated: {}, message: {}}, action) => {
  switch (action.type) {
    case FunctionEvents.INIT_READ_PAGE:
    case FunctionEvents.INIT_STATUS_PAGE:
    case FunctionEvents.INIT_LIST_PAGE:
      return Object.assign({}, state, {...action.response});
    case FunctionEvents.REFRESH_READ_PAGE:
      let entityUpdated = {...action.response},
          previousList = [...state.list],
          updatedList = [];

      for (let i = 0; i < previousList.length; i++) {
        let w = previousList[i];
        if (w.key === entityUpdated.key) {
          updatedList.push(entityUpdated);
        } else {
          updatedList.push(w);
        }
      }
      let updatedWordWithRandom = !!!entityUpdated.key ? previousList : updatedList;
      return Object.assign({}, state, {list: [...updatedWordWithRandom]}, {entityUpdated: {...action.response}});
    case FunctionEvents.GET_WORDS_RANDOMLY:
    case FunctionEvents.GET_WORDS_STATUS:
      return Object.assign({}, state, {list: [...action.response]});
    case FunctionEvents.GET_WORDS_LIST:
      let list = [...action.response].reduce(getCallbackfn(), new Map());
      return Object.assign({}, state, {list: list});
    default:
      return state;
  }
};

export default word;
