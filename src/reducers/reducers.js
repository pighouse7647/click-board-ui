import job from './job';
import word from './word';

export const reducers = {
  job,
  word,
};