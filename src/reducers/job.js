import {JobEvents} from '../constants/Events';

let job;
job = (state = {list: [], entityUpdated: {}, message: {}}, action) => {
  switch (action.type) {
    case JobEvents.INIT_JOB_LIST_PAGE:
      return Object.assign({}, state, {...action.response});
    case JobEvents.GET_JOB_RECOMMEND_DATA:
      return Object.assign({}, state, {...action.response});
    default:
      return state;
  }
};

export default job;
