import React from 'react';
import {connect} from 'react-redux';
import Table from 'react-bootstrap/Table';
import {
  getStatusWords
} from '../actions/functionActions';
import {FunctionEvents} from '../constants/Events';
import {FunctionUris} from '../constants/Uris';
import Button from 'react-bootstrap/Button';

class FunctionStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      words: [],

    };
  }

  componentDidMount() {
    this.props.dispatch(getStatusWords(FunctionEvents.INIT_STATUS_PAGE, FunctionUris.GET_WORDS_STATUS + `/${userId}`,
        {}));
  }

  componentWillReceiveProps(nextProps) {
    console.log('willReceive', nextProps.data);
    if (nextProps.data) {
      this.setState(Object.assign({}, this.state, {
        words: nextProps.data.list,
      }));
    }
  }

  render() {
    return <div className={'col-12'}>
      <div className={'row'} style={{overflowY: 'auto'}}>
        <div className={'row d-flex justify-content-center'} style={{height: '800px', marginLeft: '0px', marginRight: '0px'}}>
          <Table striped hover bordered variant={'light'} style={{tableLayout: 'fixed'}} size={'sm'}>
            <thead className={'thead-dark'}>
            <tr>
              <th style={{width: '3%'}}></th>

            </tr>
            </thead>
            <tbody>
            <div id="words" className="words-container">
              {this.state.words.size === 0 ? '<></>' : this.state.words.map((word, i) => {
                return (<Button key={i}>{word.key}
                  <span>{word.count}</span>
                  <span>{word.currentCount}</span>
                </Button>);
              })}
            </div>
            </tbody>
          </Table>
        </div>
      </div>
    </div>;
  }

  updateState(state) {
    this.setState(state, () => {
      // this.props.dispatch(getRecommendJobs(JobEvents.INIT_JOB_LIST_PAGE, this.state.url, this.getParams()));
    });
  }

  getParams() {
    return {
      // keyword: this.state.keyword,
      // area: this.state.area,
      // page: this.state.page,
      // revealPageStart: this.state.revealPageStart,
      // revealPageEnd: this.state.revealPageEnd,
      // kwop: this.state.kwop,
      // order: this.state.order,
      // asc: this.state.asc,
      // mode: this.state.mode,
      // ro: this.state.ro,
    };
  }

  elementCreator(type = 'div', innerHTML = '', className = '') {
    let tempElement = document.createElement(type);
    tempElement.innerHTML = innerHTML;
    tempElement.setAttribute('class', className);
    return tempElement;
  }
}

const mapStateToProps = (state) => {
  // console.log('mapStateToProps', state);
  return {
    data: state.word
  };
};
export default connect(mapStateToProps, null)(FunctionStatus);
