import React from 'react';
import {connect} from 'react-redux';
// import '../css/News.css';
// import logo from '../images/logo.svg';

class News extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // url: '/jobs',
      // jobs: [],
      // keyword: 'java 工程師',
      // page: '1',
      // area: ['6001001005', '6001001001', '6001001007'],
      // kwop: '7',
      // order: '1',
      // asc: '0',
      // mode: 's',
    };
  }

  componentDidMount() {
    // this.props.dispatch(getRecommendJobs(JobEvents.INIT_JOB_LIST_PAGE, this.state.url,
    //     {
    //       keyword: this.state.keyword,
    //       area: this.state.area,
    //       page: this.state.page,
    //       jobsource: '2018indexpoc',
    //       kwop: this.state.kwop,
    //       order: this.state.order,
    //       asc: this.state.asc,
    //       mode: this.state.mode
    //     }));
  }

  componentWillReceiveProps(nextProps) {
    // // console.log('willReceive', nextProps);
    // if (nextProps.data) {
    //   // console.log('do something')
    //   this.setState(Object.assign({}, this.state, {
    //     jobs: nextProps.data.list,
    //     // keyword: nextProps.data.keyword
    //   }));
    // }
  }

  render() {
    return <div className="News col-12">
      <header className="News-header">
        <img src={logo} className="News-logo" alt="logo"/>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
            className="News-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>;
  }

}

const mapStateToProps = (state) => {
  // console.log('mapStateToProps', state);
  return {
    data: state.job.data
  };
};
export default connect(mapStateToProps, null)(News);
