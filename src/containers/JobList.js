import React from 'react';
import {getRecommendJobs} from '../actions/jobActions';
import {JobEvents} from '../constants/Events';
import {connect} from 'react-redux';
import JobComponent from '../component/JobComponent';
import CommonPagination from '../component/CommonPagination';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import {FormControl} from 'react-bootstrap';

class JobList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '/jobs',
      jobs: [],
      keyword: 'java 工程師',
      page: '1',
      totalPage: '0',
      revealPageStart: '1',
      revealPageEnd: '10',
      area: ['6001001005', '6001001001', '6001001007'],
      kwop: '7',
      order: '14',
      asc: '0',
      mode: 's',
      ro: '0',
    };
  }

  componentDidMount() {
    this.props.dispatch(getRecommendJobs(JobEvents.INIT_JOB_LIST_PAGE, this.state.url,
        {
          keyword: this.state.keyword,
          area: this.state.area,
          page: this.state.page,
          kwop: this.state.kwop,
          order: this.state.order,
          asc: this.state.asc,
          mode: this.state.mode,
          ro: this.state.ro,
        }));
  }

  componentWillReceiveProps(nextProps) {
    console.log('willReceive', nextProps.data);
    if (nextProps.data) {
      // console.log('do something')
      this.setState(Object.assign({}, this.state, {
        jobs: nextProps.data.list,
        totalPage: nextProps.data.totalPage,
        // keyword: nextProps.data.keyword
      }));
    }
  }

  render() {
    let keyword = this.state.keyword,
        page = this.state.page,
        area = this.state.area;

    return <div className={'col-12'}>
      <div className={'row'}>
        <Form>
          <InputGroup className={'col-12'}>
            <InputGroup className={'col-4'}>
              <InputGroup.Prepend>
                <InputGroup.Text>關鍵字</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                  value={keyword}
                  onBlur={this.keywordBlurHandler.bind(this)}
                  onChange={this.keywordChangeHandler.bind(this)}
                  className={'col-8'}
              />
            </InputGroup>
            <InputGroup className={'col-2'}>
              <InputGroup.Prepend>
                <InputGroup.Text>頁數</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                  type={'number'}
                  value={page}
                  onBlur={this.keywordBlurHandler.bind(this)}
                  onChange={this.pageChangeHandler.bind(this)}
                  className={'col-8'}
              />
            </InputGroup>
            <InputGroup className={'col-6'}>
              <InputGroup.Prepend>
                <InputGroup.Text>區碼</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                  type={'text'}
                  value={area}
                  onBlur={this.keywordBlurHandler.bind(this)}
                  onChange={this.areaChangeHandler.bind(this)}
                  className={'col-8'}
              />
            </InputGroup>
          </InputGroup>
          <br/>
        </Form>
      </div>
      <div className={'row'}>
        <CommonPagination
            pageItemNavigatorExtHandler={this.pageItemNavigatorExtHandler.bind(this)}
            pageItemNavigatorHandler={this.pageItemNavigatorHandler.bind(this)}
            pageItemChangeHandler={this.pageItemChangeHandler.bind(this)}
            start={this.state.revealPageStart}
            end={this.state.revealPageEnd}
            active={this.state.page}
        />
      </div>
      <div className={'row'} style={{overflowY: 'auto'}}>
        <div className={'row d-flex justify-content-center'} style={{height: '800px', marginLeft: '0px', marginRight: '0px'}}>
          <Table striped hover bordered variant={'light'} style={{tableLayout: 'fixed'}} size={'sm'}>
            <thead className={'thead-dark'}>
            <tr>
              <th style={{width: '3%'}}>序號</th>
              <th style={{width: '20%'}}>公司名稱</th>
              <th style={{width: '20%'}}>職務名稱</th>
              <th style={{width: '5%'}}>區域</th>
              <th style={{width: '20%'}}>地址</th>
            </tr>
            </thead>
            <tbody>
            {this.state.jobs.map((job, i) => <JobComponent key={i} {...Object.assign({}, {...job}, {rowSeq: i})} />)}
            </tbody>
          </Table>
        </div>
      </div>
      <div className={'row'}>
        <CommonPagination
            pageItemNavigatorExtHandler={this.pageItemNavigatorExtHandler.bind(this)}
            pageItemNavigatorHandler={this.pageItemNavigatorHandler.bind(this)}
            pageItemChangeHandler={this.pageItemChangeHandler.bind(this)}
            start={this.state.revealPageStart}
            end={this.state.revealPageEnd}
            active={this.state.page}
        />
      </div>
    </div>;
  }

  updateState(state) {
    this.setState(state, () => {
      this.props.dispatch(getRecommendJobs(JobEvents.INIT_JOB_LIST_PAGE, this.state.url, this.getParams()));
    });
  }

  getParams() {
    return {
      keyword: this.state.keyword,
      area: this.state.area,
      page: this.state.page,
      revealPageStart: this.state.revealPageStart,
      revealPageEnd: this.state.revealPageEnd,
      kwop: this.state.kwop,
      order: this.state.order,
      asc: this.state.asc,
      mode: this.state.mode,
      ro: this.state.ro,
    };
  }

  keywordBlurHandler() {
    // console.log('event target', e.target)
    this.props.dispatch(getRecommendJobs(JobEvents.INIT_JOB_LIST_PAGE, this.state.url, this.getParams()));
  }

  keywordChangeHandler(e) {
    let state = Object.assign({}, this.state, {keyword: e.target.value});
    this.setState(state);
  }

  pageChangeHandler(e) {
    let state = Object.assign({}, this.state, {page: e.target.value});
    this.setState(state);
  }

  pageItemChangeHandler(e) {
    let state = Object.assign({}, this.state, {page: e.target.innerHTML});
    this.setState(state, () => {
      this.props.dispatch(getRecommendJobs(JobEvents.INIT_JOB_LIST_PAGE, this.state.url, this.getParams()));
    });
  }

  pageItemNavigatorHandler(e) {
    e.preventDefault();
    // console.log('Target', e.target)
    let
        number = (e.target.tagName === 'SPAN' ? e.target.nextSibling : e.target.querySelectorAll('span.sr-only')[0]).innerHTML === 'Next' ? 1 : -1,
        page = parseInt(this.state.page) + number,
        revealPageStart = parseInt(this.state.revealPageStart) + number,
        revealPageEnd = parseInt(this.state.revealPageEnd) + number,
        state = Object.assign({}, this.state,
            {page: page, revealPageStart: revealPageStart, revealPageEnd: revealPageEnd});

    this.updateState(state);
  }

  pageItemNavigatorExtHandler(e) {
    e.preventDefault();
    // console.log('Target', e.target)
    let
        number = (e.target.tagName === 'SPAN' ? e.target.nextSibling : e.target.querySelectorAll('span.sr-only')[0]).innerHTML === 'First' ? 1
            : this.state.totalPage,
        page = parseInt(number),
        revealPageStart = page === 1 ? 1 : page - 10,
        revealPageEnd = page === 1 ? 10 : page,
        state = Object.assign({}, this.state,
            {page: page, revealPageStart: revealPageStart, revealPageEnd: revealPageEnd});
    this.updateState(state);

  }

  areaChangeHandler(e) {
    let state = Object.assign({}, this.state, {area: e.target.value});
    this.setState(state);
  }
}

const mapStateToProps = (state) => {
  // console.log('mapStateToProps', state);
  return {
    data: state.job.data
  };
};
export default connect(mapStateToProps, null)(JobList);
