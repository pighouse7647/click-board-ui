import React from 'react';
import {connect} from 'react-redux';
import Table from 'react-bootstrap/Table';
import {
  getRandomReadWords,
  incrementWordClick
} from '../actions/functionActions';
import {FunctionEvents} from '../constants/Events';
import {FunctionUris} from '../constants/Uris';
import Button from 'react-bootstrap/Button';

class FunctionRead extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      words: [],

    };
  }

  componentDidMount() {
    this.props.dispatch(getRandomReadWords(FunctionEvents.INIT_READ_PAGE, FunctionUris.GET_WORDS_RANDOMLY + `/${userId}`,
        {}));
  }

  componentWillReceiveProps(nextProps) {
    // console.log('willReceive', nextProps.data);
    if (nextProps.data) {
      let entityUpdated = nextProps.data.entityUpdated,
          list = nextProps.data.list,
          updatedList = [];

      for(let i = 0; i < list.length; i++) {
          let w = list[i];
        if(w.key === entityUpdated.key) {
          updatedList.push(entityUpdated);
        } else {
          updatedList.push(w);
        }
      }
      let updatedWordWithRandom = !!!entityUpdated.key ? list : updatedList;

      this.setState(Object.assign({}, this.state, {
        words: updatedWordWithRandom,
      }));
    }
  }

  render() {
    let firstContent = ((this.state.words || [])[0] || {});
    // console.log(firstContent);

    return <div className={'col-12'}>
      <div className={'row'} style={{overflowY: 'auto'}}>
        <div className={'row d-flex justify-content-center'} style={{height: '800px', marginLeft: '0px', marginRight: '0px'}}>
          <Table striped hover bordered variant={'light'} style={{tableLayout: 'fixed'}} size={'sm'}>
            <thead className={'thead-dark'}>
            <tr>
              <th style={{width: '10%'}}>Name</th>
              <th style={{width: '90%'}}>Content</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>
                <div>{firstContent.userName}</div>
              </td>
              <td>
                {this.state.words.size === 0 ? '<></>' : this.state.words.map((word, i) => {
                  return (<Button key={i} onClick={this.onClickHandler.bind(this)}>{word.key.replace(/(\w+)#[\d]+/, '$1')}
                  </Button>);
                })}
              </td>
            </tr>
            </tbody>
          </Table>
          <Table striped hover bordered variant={'light'} style={{tableLayout: 'fixed'}} size={'sm'}>
            <thead className={'thead-dark'}>
            <tr>
              <th style={{width: '10%'}}>Word</th>
              <th style={{width: '45%'}}>Count</th>
              <th style={{width: '45%'}}>Current Count</th>
            </tr>
            </thead>
            <tbody>
            {this.state.words.size === 0 ? '<></>' : this.state.words.map((word, i) => {
              return (<tr>
                <td>{word.key.replace(/(\w+)#[\d]+/, '$1')}</td>
                <td>{word.count}</td>
                <td>{word.currentCount}</td>
              </tr>);
            })}
            </tbody>
          </Table>
        </div>
      </div>
    </div>;
  }

  updateState(state) {
    this.setState(state, () => {
      // this.props.dispatch(getRecommendJobs(JobEvents.INIT_JOB_LIST_PAGE, this.state.url, this.getParams()));
    });
  }

  getParams() {
    return {
      // keyword: this.state.keyword,
      // area: this.state.area,
      // page: this.state.page,
      // revealPageStart: this.state.revealPageStart,
      // revealPageEnd: this.state.revealPageEnd,
      // kwop: this.state.kwop,
      // order: this.state.order,
      // asc: this.state.asc,
      // mode: this.state.mode,
      // ro: this.state.ro,
    };
  }

  elementCreator(type = 'div', innerHTML = '', className = '') {
    let tempElement = document.createElement(type);
    tempElement.innerHTML = innerHTML;
    tempElement.setAttribute('class', className);
    return tempElement;
  }

  onClickHandler(e) {
    e.preventDefault();
    let word = this.state.words.filter(w => w.key.replace(/(\w+)#[\d]+/, '$1') === e.target.innerHTML)[0];
    this.props.dispatch(incrementWordClick(FunctionEvents.REFRESH_READ_PAGE, FunctionUris.UPDATE_WORDS_CLICK, {...word}));
  }
}

const mapStateToProps = (state) => {
  // console.log('mapStateToProps', state);
  return {
    data: state.word
  };
};
export default connect(mapStateToProps, null)(FunctionRead);
