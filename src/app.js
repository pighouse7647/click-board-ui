import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {CommonEvents} from './constants/Events';
import CustomizedRouter from './component/CustomizedRouter';
import {
  applyMiddleware,
  combineReducers,
  createStore
} from 'redux';
import {createLogger} from 'redux-logger';

import thunk from 'redux-thunk';
import stateShare from './middlewares/stateShare';
import {routerReducer} from 'react-router-redux';
import {reducers} from './reducers/reducers';
import './css/index.css';

window.app = {};
export const store = createStore(
    combineReducers(
        {...reducers, router: routerReducer}), // Registry reducers
    applyMiddleware(
        thunk, // For the purpose of Async
        stateShare, // For temporary cache multiple reducers' state.
        createLogger(), // Logger like action: before, after...
    ));

/*global app:true*/
app.create = (context, dom) => {
  store.dispatch({type: CommonEvents.INIT});
  app.run(dom);
};

app.run = (dom) => {
  ReactDOM.render(
      <Provider store={store}>
        <div className="App">
          <header className="App-header">
            <CustomizedRouter/>
          </header>
        </div>
      </Provider>,
      dom);
};

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
