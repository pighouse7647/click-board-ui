import React from 'react';
import ReactDOM from 'react-dom';
import CustomizedRouter from './component/CustomizedRouter';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<div className="App">
    <header className="App-header">
      <CustomizedRouter/>
    </header>
  </div>, div);
  ReactDOM.unmountComponentAtNode(div);
});
