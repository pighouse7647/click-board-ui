import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import {Route} from 'react-router-dom';

export default class PrivateRoute extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // if (typeof (this.props.initBodyRoot) != 'undefined') {
    //   this.props.initBodyRoot();
    // }
  }

  componentDidUpdate() {
    // if (typeof (this.props.initBodyRoot) != 'undefined') {
    //   this.props.initBodyRoot();
    // }
  }
  render() {
    const {component, ...rest} = this.props;
    return (
        <Route {...rest} render={props => {
          let newProps = {...props, ...rest};
          return React.createElement(component, newProps);
        }}/>
    );
  }
}

PrivateRoute.propTypes = {
  rest: PropTypes.any,
};