import Pagination from 'react-bootstrap/Pagination';
import * as PropTypes from 'prop-types';
import React from 'react';

export default class CommonPagination extends React.Component {
  render() {

    let items = [];
    for (let number = this.props.start; number <= this.props.end; number++) {
      items.push(
          <Pagination.Item key={number} active={parseInt(number) === parseInt(this.props.active)} onClick={this.props.pageItemChangeHandler}>
            {number}
          </Pagination.Item>,
      );
    }

    return (
        <div className={'d-flex justify-content-center mb-5 mt-auto col-12'}>
          <div className={'position-absolute'}>
          <Pagination size="sm">
            <Pagination.First onClick={this.props.pageItemNavigatorExtHandler}/>
            <Pagination.Prev onClick={this.props.pageItemNavigatorHandler}/>
            {items}
            <Pagination.Ellipsis/>
            <Pagination.Next onClick={this.props.pageItemNavigatorHandler}/>
            <Pagination.Last onClick={this.props.pageItemNavigatorExtHandler}/>
          </Pagination>
          </div>
        </div>
    );
  }
}

CommonPagination.propTypes = {
  pageItemNavigatorExtHandler: PropTypes.any,
  pageItemNavigatorHandler: PropTypes.any,
  pageItemChangeHandler: PropTypes.any,
  active: PropTypes.any,
  start: PropTypes.any,
  end: PropTypes.any,
};