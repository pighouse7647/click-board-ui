import React from 'react';
import PropTypes from 'prop-types';

export default class JobComponent extends React.Component {

  render() {
    // console.log(this.props.rowSeq)
    return (<tr>
      <td>{this.props.rowSeq+1}</td>
      <td><a href={`http:${this.props.link.cust}`}>{this.props.custName}</a></td>
      <td><a href={`http:${this.props.link.job}`}>{this.props.jobName}</a></td>
      <td>{this.props.jobAddrNoDesc}</td>
      <td>{this.props.jobAddress}</td>
    </tr>);
  }

}
JobComponent.propTypes = {
  jobName: PropTypes.string,
  jobAddrNoDesc: PropTypes.string,
  jobAddress: PropTypes.string,
  custName: PropTypes.string,
  rowSeq: PropTypes.number,
};