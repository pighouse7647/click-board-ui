import React from 'react';

export default class Footer extends React.Component {
  render() {
    return <>
      <div className="mx-4 mt-3 mb-2 col-10 footer-text">Click Board UI</div>
      <div className="mx-4 my-2 col-10 footer-text">Address</div>
      <div className="mx-4 my-2 col-10 footer-text">Tel</div>
      <div className="mx-4 my-3 col-10 footer-text"></div>
    </>;
  }
}