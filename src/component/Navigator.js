import React, {Component} from 'react';
import {
  Nav,
  Navbar,
  NavDropdown
} from 'react-bootstrap';
import {
  CommonUris,
  FunctionUris
} from '../constants/Uris';
import {LinkContainer} from 'react-router-bootstrap';

export default class Navigator extends Component {
  render() {
    return <Navbar className="bd-navbar col-12">
      <Navbar.Brand>Click Board UI</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav"/>
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <LinkContainer to={CommonUris.HOME_PAGE}>
            <Nav.Link>Home</Nav.Link>
          </LinkContainer>
          <NavDropdown title="Function Pages" id="basic-nav-dropdown">
            <LinkContainer to={FunctionUris.READ_PAGE}>
              <NavDropdown.Item>Read</NavDropdown.Item>
            </LinkContainer>
            <LinkContainer to={FunctionUris.STATUS_PAGE}>
              <NavDropdown.Item>Status</NavDropdown.Item>
            </LinkContainer>
            <LinkContainer to={FunctionUris.LIST_PAGE}>
              <NavDropdown.Item>List</NavDropdown.Item>
            </LinkContainer>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>;
  }
}