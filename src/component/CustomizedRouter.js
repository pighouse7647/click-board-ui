import React from 'react';
import {
  BrowserRouter,
  HashRouter
} from 'react-router-dom';

import JobList from '../containers/JobList';
import {store} from '../app';
import PrivateRoute from './PrivateRoute';
import Navigator from './Navigator';

import 'bootstrap/dist/css/bootstrap.css';
import {
  CommonUris,
  FunctionUris,
  JobUris
} from '../constants/Uris';
import Container from 'react-bootstrap/Container';
import Footer from './Footer';
import FunctionRead from '../containers/FunctionRead';
import Home from '../containers/Home';
import FunctionStatus from '../containers/FunctionStatus';
import FunctionList from '../containers/FunctionList';

export default class CustomizedRouter extends React.Component {
  render() {
    return (
        <HashRouter>
          <Container fluid={true} className={'col-10'}>
            <div className="row grid-2">
              <Navigator/>
            </div>
            <div className="row grid-2"/>
            <div className="row">
              <PrivateRoute path={CommonUris.HOME_PAGE} component={Home} dispatch={store.dispatch}/>
            </div>
            <div className="row">
              <PrivateRoute path={JobUris.LIST_PAGE} component={JobList} dispatch={store.dispatch}/>
            </div>
            <div className="row">
              <PrivateRoute path={FunctionUris.READ_PAGE} component={FunctionRead} dispatch={store.dispatch}/>
              <PrivateRoute path={FunctionUris.STATUS_PAGE} component={FunctionStatus} dispatch={store.dispatch}/>
              <PrivateRoute path={FunctionUris.LIST_PAGE} component={FunctionList} dispatch={store.dispatch}/>
            </div>
            <div className="row footer-bar">
              <Footer/>
            </div>
          </Container></HashRouter>
    );
  }
}