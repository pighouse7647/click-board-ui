import {JobEvents} from '../constants/Events';
import {getAll} from './commonActions';


export const  getRecommendJobs = (type, url, params) => {
  return (dispatch) => {
    return new Promise(res => dispatch(getAll(JobEvents.GET_JOB_RECOMMEND_DATA, url, params, res)))
    // Pass the beginning action(and data) to stateShare through dispatch, means finished the whole action.
    .then(response => dispatch({type, response}))
    .catch(console.error);
  };
};