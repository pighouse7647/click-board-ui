import {FunctionEvents} from '../constants/Events';
import {
  add,
  getAll
} from './commonActions';
import {FunctionUris} from '../constants/Uris';

export const getRandomReadWords = (type, url, params) => {
  return (dispatch) => {
    return new Promise(res => dispatch(getAll(FunctionEvents.GET_WORDS_RANDOMLY, url, params, res)))
    // Pass the beginning action(and data) to stateShare through dispatch, means finished the whole action.
    .then(response => dispatch({type, response}))
    .catch(console.error);
  };
};

export const getStatusWords = (type, url, params) => {
  return (dispatch) => {
    return new Promise(res => dispatch(getAll(FunctionEvents.GET_WORDS_STATUS, url, params, res)))
    // Pass the beginning action(and data) to stateShare through dispatch, means finished the whole action.
    .then(response => dispatch({type, response}))
    .catch(console.error);
  };
};
export const getListWords = (type, url, params) => {
  return (dispatch) => {
    return new Promise(res => dispatch(getAll(FunctionEvents.GET_WORDS_LIST, url, params, res)))
    // Pass the beginning action(and data) to stateShare through dispatch, means finished the whole action.
    .then(response => dispatch({type, response}))
    .catch(console.error);
  };
};

export const incrementWordClick = (type, url, params) => {
  return (dispatch) => {
    return new Promise(res => dispatch(add(FunctionEvents.UPDATE_WORDS_CLICK, url, params, res)))
    // .then(response => {
    //   return new Promise((res) => dispatch(
    //       getAll(FunctionEvents.GET_WORDS_STATUS, FunctionUris.GET_WORDS_STATUS + `/${response.userId}`, {}, res)));
    // })
    // Pass the beginning action(and data) to stateShare through dispatch, means finished the whole action.
    .then(response => dispatch({type, response}))
    .catch(console.error);
  };
};