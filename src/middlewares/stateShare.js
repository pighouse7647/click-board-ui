import {
  FunctionEvents,
  JobEvents
} from '../constants/Events';

export default store => next => action => {
  const state = store.getState() || {list: [], entityUpdated: {}, message: {}};
  const {type} = action;

  switch (type) {
    case JobEvents.INIT_JOB_LIST_PAGE:
      action.pageResponse = {
        status: 200,
        data: {
          job: state.job.data,

        }
      };
      break;
      case FunctionEvents.INIT_READ_PAGE:
      case FunctionEvents.INIT_STATUS_PAGE:
      action.pageResponse = {
        status: 200,
        data: {
          word: state.word.data,

        }
      };
      break;
    default:

      break;

  }
  next(action);
}