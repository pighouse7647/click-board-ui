const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require("../webpack.config");

const cfg = config;

const proxy = process.env.npm_config_proxy || "http://localhost:8080/";
console.log('Start proxy %s to http://localhost:3000/...', proxy);

const skipDevtool = process.env.npm_config_skipDevtool || "false";
if(skipDevtool !== "true") {
  console.log("devtool is enable, if you want to disable devtool add the '--skipDevtool' in your command");
  cfg.devtool = 'eval-source-map';
}

const port = 3000;

const timeout = 240000;

const options = {
  proxy: {
    '**': {
      target: proxy,
      secure: false,
      prependPath: false,
      proxyTimeout: timeout,
      onProxyReq: (proxyReq, req, res) => req.setTimeout(timeout)
    }
  },
  publicPath: '/',
  historyApiFallback: true,
  contentBase: './',
  hot: true
};

const server = new WebpackDevServer(webpack(config), options);

server.listen(port, 'localhost', function (err) {
  if (err) {
    console.log(err);
  }
  console.log('WebpackDevServer listening at localhost:', port);
});